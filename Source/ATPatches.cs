﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using Verse;

namespace AndroidTiersReheated
{
    /// <summary>
    /// Oh God in Heaven.
    /// </summary>
    [StaticConstructorOnStartup]
    public static class ATPatches
    {
        private static readonly Type self = typeof(ATPatches);
        static ATPatches()
        {
            var harmony = new Harmony(id: "N3X15.AndroidTiers.Reheated");
            // https://github.com/Atla55/Android-Tiers-Core/blob/b15a7f345d3b8bbf5bd7f6b9605a5b47f3c1de0d/Source/Androids%20For%20RW1.3/Components/CompHeatSensitive.cs#L121
            harmony.Patch(AccessTools.Method(typeof(MOARANDROIDS.CompHeatSensitive), "CheckTemperature"), transpiler: new HarmonyMethod(self, nameof(CompHeatSensitive_CheckTemperature)));
        }

        public static IEnumerable<CodeInstruction> CompHeatSensitive_CheckTemperature(IEnumerable<CodeInstruction> instructions, ILGenerator gen)
        {
            // All we're looking for:
            //float ambientTemperature = this.parent.AmbientTemperature;
            //27    003C    ldarg.0
            //28    003D    ldfld    class ['Assembly-CSharp']Verse.ThingWithComps ['Assembly-CSharp']Verse.ThingComp::parent
            //29    0042    callvirt instance float32 ['Assembly-CSharp']Verse.Thing::get_AmbientTemperature()
            //30    0047    stloc.1
            var getTemp = AccessTools.PropertyGetter(typeof(Verse.Thing), nameof(Verse.Thing.AmbientTemperature));
            var getRoomTemp = AccessTools.PropertyGetter(typeof(Verse.Room), nameof(Verse.Room.Temperature));
            var mGetRoom = AccessTools.Method(typeof(Verse.RegionAndRoomQuery), nameof(Verse.RegionAndRoomQuery.GetRoom));
            var instructionList = instructions.ToList();
            for (int i = 0; i < instructionList.LongCount(); i++)
            {
                var instr = instructionList[i];
                if (instr.Is(OpCodes.Callvirt, getTemp))
                {
                    // float ambientTemperature = this.parent.GetRoom().Temperature;
                    // We're already up to parent.
                    yield return new CodeInstruction(OpCodes.Ldc_I4_S, 15); // All regions
                    yield return new CodeInstruction(OpCodes.Call, mGetRoom);
                    yield return new CodeInstruction(OpCodes.Callvirt, getRoomTemp);
                }
                else
                {
                    yield return instr;
                }
            }
        }
    }
}