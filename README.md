# Android Tiers - Reheated

A RimWorld Mod by N3X15 that keeps [Android Tiers](https://steamcommunity.com/sharedfiles/filedetails/?id=1386412863) SkyCloud Cores from exploding when you also use the mod [Celsius](https://steamcommunity.com/sharedfiles/filedetails/?id=2725863762).

## Wait what

Android Tiers is a mod that adds various forms and flavors of android to the game RimWorld, so you can act out all your Terminator-on-tribal war-crime fantasies. (Yes, there's a terminator mod.  Yes, it does what it says on the tin.) 

Part of Android Tiers is SkyCloud, a system of hooking up all your androids to the internet, with all the virus-filled fun that naturally entails.  A crucial part of this system is the SkyCloud Core.  Cores house the uploaded personalities of your original colonists, and, because they're giant masses of angry pixies in 60 tons of copper and steel, need to be cooled at all times.

Celsius is a more recent mod that allows for weather and ambient air temperature to cause ice to form or melt, and also improves how thermodynamics work in RimWorld.

Celsius also causes SkyCloud Cores to immediately explode.

### HOW

SkyCloud Cores use a simple check of ambient air temperature.  Due to the changes Celsius introduces, ambient air temperature is not working the same as it did before, and this causes crazy temperatures to be reported.  Because SkyCloud Cores expect temperatures to be under 5°C, they immediately start screeching and catching fire, even if the room is at -20°C.

## The Fix

This is a small mod that jumps through some technological hoops to, when it's all boiled down, do the following:

1. Tell the Core it is, in fact, a big honking chunk of steel.  1 metric ton of it, to be precise. It also tells it to (thermodynamically-speaking) *act* like it.
2. Change from using the possibly-broken or unused ambient air temperature check to using a check of ambient *room* temperature.  If a room is not sealed, it will use the outdoor map temperature.
   
### Nerd Speak

* Adds the needed thermal properties that Celsius expects.
* Performs a small IL patch to `CompHeatSensitive` to change `float ambientTemperature = this.parent.AmbientTemperature;` to `float ambientTemperature = this.parent.GetRoom().Temperature;` in `CheckTemperature()`. A Harmony pre/postfix was not feasible due to the complexity and structure of the method in question.

## Compatibility
* Requires Android Tiers and Celsius
* No known incompatibilities
* Should not break saves when added or removed.

## Installing

* Download the zip from [releases](https://gitlab.com/N3X15/rimworld-android-tiers-reheated/-/releases).
* Extract it into your Mods/ folder.
* Make sure that it's loaded after Android Tiers and Celsius.

Everything should Just Work™.

**DO NOT CLONE OR OTHERWISE DOWNLOAD THIS REPO BRANCH DIRECTLY TO YOUR MODS FOLDER. THINGS ARE MISSING SO IT WILL NOT WORK.** DLLs were removed to reduce repo size and complexity, and unnecessary junk used during development will be present.

## Compiling

I am a bit weird so I have a big complicated build process.

1. Install dotnet SDK 6 or whatever the hell the latest stable/LTS build is. NOT the one marked RC.
2. Install the .NET Framework 4.8 Developer Pack from [here](https://dotnet.microsoft.com/en-us/download/visual-studio-sdks) (Scroll down)
3. Install Python 3.10.
4. Install git.
5. `pip install -U poetry`
6. `git clone https://gitlab.com/N3X15/rimworld-android-tiers-reheated.git Android-Tiers-Reheated`
7. Patch Source/ATR.csproj so all the paths point to the right DLLs.
8. `cd Android-Tiers-Reheated`
9. `cd Source`
10. `dotnet build`
11. `cd ..`
12. `poetry run python devtools/deploy.py --deploy-to="C:\path\to\RimWorld\Mods\Android Tiers Reheated"`

There, now you have a nice, minimal install with no junk in it.  Enjoy.

## Licensing

This software is available to you under the terms of the MIT Open Source License.  See [LICENSE](./LICENSE).

tl;dr:
* If your computer blows up: not my problem
* If this shit don't work: not my problem (though you should tell me anyway so I can fix it eventually)
* Modification of my shitcode: Permitted, leave up all the licensing and credit my past works
* Use my shitcode in your shitcode: Welcome, but credit me
* Sharing my shitcode with your friends on other sites: Sure, but credit me
* Tell everyone this is YOUR shitcode: I will eat you. (Legally-speaking.)